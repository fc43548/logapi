<?php

use Illuminate\Database\Seeder;
Use App\LogUser;

class LogUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = \Faker\Factory::create();

        $faker->dateTimeBetween('+50 days', '+2 years');


        // Let's truncate our existing records to start from scratch.

        LogUser::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 20; $i++) {

            LogUser::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'gender' => $i%2 == 0 ? 'male' : 'female',
                'birthday' => $faker->dateTimeBetween('-50 years', '+0 days'),

            ]);
        }

    }
}
