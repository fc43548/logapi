<?php

use Illuminate\Http\Request;
Use App\LogUser;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::get('logusers', 'LogUserController@index');
Route::get('logusers/{id}', 'LogUserController@show');
Route::post('logusers', 'LogUserController@store');
Route::put('logusers/{id}', 'LogUserController@update');
Route::delete('logusers/{id}', 'LogUserController@delete');