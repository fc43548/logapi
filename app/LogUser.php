<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUser extends Model
{
    //
    protected $fillable = ['name', 'email', 'birthday', 'gender'];
}
