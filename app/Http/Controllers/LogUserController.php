<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogUser;

class LogUserController extends Controller
{
    public function index()
    {
        return LogUser::all();
    }

    public function show($id)
    {
        return LogUser::find($id);
    }

    public function store(Request $request)
    {
        return LogUser::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $logUser = LogUser::findOrFail($id);

        $logUser->update($request->all());

        return $logUser;
    }

    public function delete(Request $request, $id)
    {
        $article = LogUser::findOrFail($id);
        $article->delete();

        return 204;
    }
}
