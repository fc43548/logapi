# LogAPI



###CONFIGURE

In .env file configure your DB connection.

For build and populate the DB, execute the following commands:
 php artisan migrate
 php artisan db:seed


###USE

The path for API: "/logusers"
____
Methods       Verbs

View User    - GET

List Users   - GET

Create User  - POST

Update User  - PUT

Delete User  - DELETE
____

Resquest body for Create and Update

{
  "name": "Joao alves",
  "email": "jalves@yahoo.com",
  "birthday": "1987-07-12",
  "gender": "male"
}

For list, view and delete method "id" (userId) must be send in url. ex: "/logusers/10"

