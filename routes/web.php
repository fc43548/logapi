<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('logusers', 'LogUserController@index');
//Route::get('logusers/{id}', 'LogUserController@show');
//Route::post('logusers', 'LogUserController@store');
//Route::put('logusers/{id}', 'LogUserController@update');
//Route::delete('logusers/{id}', 'LogUserController@delete');